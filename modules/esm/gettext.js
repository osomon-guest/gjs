// SPDX-License-Identifier: MIT OR LGPL-2.0-or-later
// SPDX-FileCopyrightText: 2020 Evan Welsh <contact@evanwelsh.com>

export let {
    setlocale,
    textdomain,
    bindtextdomain,
    gettext,
    dgettext,
    dcgettext,
    ngettext,
    dngettext,
    pgettext,
    dpgettext,
    domain,
} = imports._gettext;

export default {
    setlocale,
    textdomain,
    bindtextdomain,
    gettext,
    dgettext,
    dcgettext,
    ngettext,
    dngettext,
    pgettext,
    dpgettext,
    domain,
};
